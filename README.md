## Overview

**futurae-web-ruby** - Provides the Futurae Web Ruby helpers to be integrated in your ruby web
application.

These helpers allow developers to integrate Futurae's Web authentication suite into their web apps,
without the need to implement the Futurae Auth API.

## Unit Tests

```bash
rake
```

## Documentation

Documentation on using the Futurae Web Widget can be found [here](https://futurae.com/docs/guide/futurae-web/).
