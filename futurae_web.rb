require 'openssl'
require 'base64'

##
# Futurae Web Ruby
#
module Futurae
  FUTURAE_PREFIX  = 'REQ'.freeze
  APP_PREFIX  = 'APP'.freeze
  AUTH_PREFIX = 'AUTH'.freeze

  FUTURAE_EXPIRY = 600
  APP_EXPIRY = 3600

  WID_LEN = 36
  WKEY_LEN = 40
  SKEY_LEN = 40

  ERR_USERNAME = 'ERR|The username passed to sign_request() is invalid.'.freeze
  ERR_WID = 'ERR|The Futurae Web ID passed to sign_request() is invalid.'.freeze
  ERR_WKEY = 'ERR|The Futurae Web Key passed to sign_request() is invalid.'.freeze
  ERR_SKEY = "ERR|The application Secret Key passed to sign_request() must be at least #{Futurae::SKEY_LEN} characters.".freeze

  # Sign a Futurae 2FA request
  # @param wid [String] Futurae Web ID
  # @param wkey [String] Futurae Web Key
  # @param skey [String] Futurae Secret Key
  # @param username [String] Username to authenticate
  def sign_request(wid, wkey, skey, username)
    return Futurae::ERR_USERNAME if !username || username.empty?
    return Futurae::ERR_USERNAME if username.include? '|'
    return Futurae::ERR_WID if !wid || wid.to_s.length != Futurae::WID_LEN
    return Futurae::ERR_WKEY if !wkey || wkey.to_s.length < Futurae::WKEY_LEN
    return Futurae::ERR_SKEY if !skey || skey.to_s.length < Futurae::SKEY_LEN

    vals = [wid, username]

    sig = sign_vals(wkey, vals, Futurae::FUTURAE_PREFIX, Futurae::FUTURAE_EXPIRY)
    app_sig = sign_vals(skey, vals, Futurae::APP_PREFIX, Futurae::APP_EXPIRY)

    "#{sig}:#{app_sig}"
  end

  # Verify a Futurae 2FA request
  # @param wid [String] Futurae Web ID
  # @param wkey [String] Futurae Web Key
  # @param skey [String] Futurae Secret Key
  # @param sig_response [String] Futurae response
  def verify_response(wid, wkey, skey, sig_response)
    begin
      auth_sig, app_sig = sig_response.to_s.split(':')
      auth_user = parse_vals(wkey, auth_sig, Futurae::AUTH_PREFIX, wid)
      app_user = parse_vals(skey, app_sig, Futurae::APP_PREFIX, wid)
    rescue
      return nil
    end

    return nil unless auth_user == app_user

    auth_user
  end

  private

  def hmac_sha256(key, data)
    OpenSSL::HMAC.hexdigest(OpenSSL::Digest.new('sha256'), key, data.to_s)
  end

  def sign_vals(key, vals, prefix, expire)
    exp = Time.now.to_i + expire

    val_list = vals + [exp]
    val = val_list.join('|')

    b64 = Base64.encode64(val).delete("\n")
    cookie = "#{prefix}|#{b64}"

    sig = hmac_sha256(key, cookie)

    "#{cookie}|#{sig}"
  end

  def parse_vals(key, val, prefix, wid)
    ts = Time.now.to_i

    parts = val.to_s.split('|')
    return nil unless parts.length == 3
    u_prefix, u_b64, u_sig = parts

    sig = hmac_sha256(key, "#{u_prefix}|#{u_b64}")

    return nil unless hmac_sha256(key, sig) == hmac_sha256(key, u_sig)

    return nil unless u_prefix == prefix

    cookie_parts = Base64.decode64(u_b64).to_s.split('|')
    return nil unless cookie_parts.length == 3
    u_wid, user, exp = cookie_parts

    return nil unless u_wid == wid

    return nil if ts >= exp.to_i

    user
  end

  extend self
end
