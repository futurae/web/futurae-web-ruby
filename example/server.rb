require 'thin'
require_relative '../futurae_web'

# Endpoint / view handler for responding to 2FA auth request and response.
class FuturaeAuthenticator
  def initialize
    @host = ''
    @wid = ''
    @wkey = ''
    @skey = ''
  end

  def call(env)
    request = Rack::Request.new(env)
    if request.get?
      username = request.params['username']
      sig_request = Futurae.sign_request(@wid, @wkey, @skey, username)
      body = ["
            <!DOCTYPE html>
            <html lang='en'>
              <head>
                <meta charset='UTF-8'>
                <title>Futurae Authentication</title>
                <meta name='viewport' content='width=device-width, initial-scale=1'>
                <meta http-equiv='X-UA-Compatible' content='IE=edge'>
              </head>
              <body>
                <h1>Futurae Authentication</h1>
                <form method='POST' id='futurae_form'>
                </form>
                <iframe id='futurae_widget'
                        title='Two-Factor Authentication'
                        frameborder='0'
                        data-host='#{@host}'
                        data-sig-request='#{sig_request}'
                        data-lang='en'
                        allow='microphone'>
                </iframe>
                <script src='js/Futurae-Web-SDK-v1.js'></script>
              </body>
            </html>
          "]
    elsif request.post?
      sig_response = request.params['sig_response']
      user = Futurae.verify_response(@wid, @wkey, @skey, sig_response)
      body = ["#{user} authenticated Successfully!"]
    end

    [200, {'Content-Type'=>'text/html'}, body]
  end
end

app = Rack::URLMap.new('/' => FuturaeAuthenticator.new,
                       '/js' => Rack::File.new('./'))
Thin::Server.start('0.0.0.0', 3000, app)
