require 'test/unit'
require_relative './helpers'
require_relative '../futurae_web'

WID = 'FIDXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX'.freeze
WRONG_WID = 'FIDXXXXXXXXXXXXXXXXY'.freeze
WKEY = 'futuraegeneratedsharedwebapplicationkey.'.freeze
SKEY = 'useselfgeneratedhostapplicationsecretkey'.freeze

USERNAME = 'testusername'.freeze

##
# Test Futurae Web Ruby
#
class TestFuturaeWeb < Test::Unit::TestCase
  def test_sign_request
    request_sig = Futurae.sign_request(WID, WKEY, SKEY, USERNAME)
    assert_not_equal(request_sig, nil)

    request_sig = Futurae.sign_request(WID, WKEY, SKEY, '')
    assert_equal(request_sig, Futurae::ERR_USERNAME)

    request_sig = Futurae.sign_request(WID, WKEY, SKEY, 'in|valid')
    assert_equal(request_sig, Futurae::ERR_USERNAME)

    request_sig = Futurae.sign_request('invalid', WKEY, SKEY, USERNAME)
    assert_equal(request_sig, Futurae::ERR_WID)

    request_sig = Futurae.sign_request(WID, 'invalid', SKEY, USERNAME)
    assert_equal(request_sig, Futurae::ERR_WKEY)

    request_sig = Futurae.sign_request(WID, WKEY, 'invalid', USERNAME)
    assert_equal(request_sig, Futurae::ERR_SKEY)
  end

  def test_verify_response
    request_sig = Futurae.sign_request(WID, WKEY, SKEY, USERNAME)
    _futurae_sig, valid_app_sig = request_sig.to_s.split(':')

    request_sig = Futurae.sign_request(WID, WKEY, 'invalid' * 6, USERNAME)
    _futurae_sig, invalid_app_sig = request_sig.to_s.split(':')

    invalid_user = Futurae.verify_response(WID, WKEY, SKEY, "#{Helpers.invalid_response}:#{valid_app_sig}")
    assert_equal(invalid_user, nil)

    expired_user = Futurae.verify_response(WID, WKEY, SKEY, "#{Helpers.expired_response(WID, WKEY, USERNAME)}:#{valid_app_sig}")
    assert_equal(expired_user, nil)

    future_user = Futurae.verify_response(WID, WKEY, SKEY, "#{Helpers.future_response(WID, WKEY, USERNAME)}:#{invalid_app_sig}")
    assert_equal(future_user, nil)

    future_user = Futurae.verify_response(WID, WKEY, SKEY, "#{Helpers.future_response(WID, WKEY, USERNAME)}:#{valid_app_sig}")
    assert_equal(future_user, USERNAME)

    future_user = Futurae.verify_response(WID, WKEY, SKEY, "#{Helpers.future_response(WID, WKEY, USERNAME)}:#{Helpers.bad_params_app(WID, WKEY, USERNAME)}")
    assert_equal(future_user, nil)

    future_user = Futurae.verify_response(WID, WKEY, SKEY, "#{Helpers.bad_params_response(WID, WKEY, USERNAME)}:#{valid_app_sig}")
    assert_equal(future_user, nil)

    future_user = Futurae.verify_response(WRONG_WID, WKEY, SKEY, "#{Helpers.future_response(WID, WKEY, USERNAME)}:#{valid_app_sig}")
    assert_equal(future_user, nil)
  end
end
