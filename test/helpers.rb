require 'openssl'
require 'base64'

module Helpers
	def bad_params_app(wid, skey, username)
    vals = [wid, username, _expired_expiry, 'bad_param']
    cookie = _cookie('APP', vals)
    ctx_hex = _hmac(skey, cookie)

    "#{cookie}|#{ctx_hex}"
	end

  def bad_params_response(wid, wkey, username)
    vals = [wid, username, _expired_expiry, 'bad_param']
    cookie = _cookie('AUTH', vals)
    ctx_hex = _hmac(wkey, cookie)

    "#{cookie}|#{ctx_hex}"
  end

  def expired_response(wid, wkey, username)
    vals = [wid, username, _expired_expiry]
    cookie = _cookie('AUTH', vals)
    ctx_hex = _hmac(wkey, cookie)

    "#{cookie}|#{ctx_hex}"
  end

  def future_response(wid, wkey, username)
    vals = [wid, username, _future_expiry]
    cookie = _cookie('AUTH', vals)
    ctx_hex = _hmac(wkey, cookie)

    "#{cookie}|#{ctx_hex}"
  end

  def future_enroll_response(wid, wkey, username)
    vals = [wid, username, _future_expiry]
    cookie = _cookie('ENROLL', vals)
    ctx_hex = _hmac(wkey, cookie)

    "#{cookie}|#{ctx_hex}"
  end

  def invalid_response
		'AUTH|INVALID|SIG'
  end

  private

  def _cookie(prefix, vals)
    "#{prefix}|#{Base64.encode64(vals.join('|'))}"
  end

  def _future_expiry
    (Time.now.to_i + 600).to_s
  end

  def _expired_expiry
    (Time.now.to_i - 600).to_s
  end

  def _hmac(key, data)
    OpenSSL::HMAC.hexdigest(OpenSSL::Digest.new('sha256'), key, data.to_s)
  end

  extend self
end
